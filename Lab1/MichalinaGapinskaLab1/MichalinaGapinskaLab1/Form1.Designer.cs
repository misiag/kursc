﻿namespace MichalinaGapinskaLab1
{
    partial class FormName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.buttonChange = new System.Windows.Forms.Button();
            this.labelDigit = new System.Windows.Forms.Label();
            this.textBoxDigit = new System.Windows.Forms.TextBox();
            this.textBoxNumber1 = new System.Windows.Forms.TextBox();
            this.textBoxNumber2 = new System.Windows.Forms.TextBox();
            this.textBoxNumber3 = new System.Windows.Forms.TextBox();
            this.buttonCount = new System.Windows.Forms.Button();
            this.labelPlus = new System.Windows.Forms.Label();
            this.labelEqual = new System.Windows.Forms.Label();
            this.timerCounter = new System.Windows.Forms.Timer(this.components);
            this.textBoxTimer = new System.Windows.Forms.TextBox();
            this.buttonNewWindowShow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.BackColor = System.Drawing.Color.LightGreen;
            this.buttonStart.Font = new System.Drawing.Font("Courier New", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonStart.Location = new System.Drawing.Point(84, 85);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(156, 60);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = false;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonStop.Location = new System.Drawing.Point(84, 151);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(156, 69);
            this.buttonStop.TabIndex = 2;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(72, 13);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(553, 69);
            this.labelName.TabIndex = 3;
            this.labelName.Text = "Michalina Gapińska";
            // 
            // textBoxResult
            // 
            this.textBoxResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxResult.Location = new System.Drawing.Point(84, 226);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(223, 45);
            this.textBoxResult.TabIndex = 4;
            this.textBoxResult.Text = "Komunikat";
            // 
            // buttonChange
            // 
            this.buttonChange.Location = new System.Drawing.Point(84, 352);
            this.buttonChange.Name = "buttonChange";
            this.buttonChange.Size = new System.Drawing.Size(136, 36);
            this.buttonChange.TabIndex = 5;
            this.buttonChange.Text = "buttonChange";
            this.buttonChange.UseVisualStyleBackColor = true;
            this.buttonChange.Click += new System.EventHandler(this.buttonChange_Click);
            // 
            // labelDigit
            // 
            this.labelDigit.AutoSize = true;
            this.labelDigit.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDigit.Location = new System.Drawing.Point(77, 276);
            this.labelDigit.Name = "labelDigit";
            this.labelDigit.Size = new System.Drawing.Size(125, 39);
            this.labelDigit.TabIndex = 6;
            this.labelDigit.Text = "Liczba:";
            // 
            // textBoxDigit
            // 
            this.textBoxDigit.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxDigit.Location = new System.Drawing.Point(208, 276);
            this.textBoxDigit.Name = "textBoxDigit";
            this.textBoxDigit.Size = new System.Drawing.Size(99, 45);
            this.textBoxDigit.TabIndex = 7;
            // 
            // textBoxNumber1
            // 
            this.textBoxNumber1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNumber1.Location = new System.Drawing.Point(450, 105);
            this.textBoxNumber1.Name = "textBoxNumber1";
            this.textBoxNumber1.Size = new System.Drawing.Size(100, 36);
            this.textBoxNumber1.TabIndex = 8;
            // 
            // textBoxNumber2
            // 
            this.textBoxNumber2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNumber2.Location = new System.Drawing.Point(590, 105);
            this.textBoxNumber2.Name = "textBoxNumber2";
            this.textBoxNumber2.Size = new System.Drawing.Size(100, 36);
            this.textBoxNumber2.TabIndex = 9;
            // 
            // textBoxNumber3
            // 
            this.textBoxNumber3.Enabled = false;
            this.textBoxNumber3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNumber3.Location = new System.Drawing.Point(730, 105);
            this.textBoxNumber3.Name = "textBoxNumber3";
            this.textBoxNumber3.Size = new System.Drawing.Size(100, 36);
            this.textBoxNumber3.TabIndex = 10;
            // 
            // buttonCount
            // 
            this.buttonCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCount.Location = new System.Drawing.Point(534, 160);
            this.buttonCount.Name = "buttonCount";
            this.buttonCount.Size = new System.Drawing.Size(156, 49);
            this.buttonCount.TabIndex = 11;
            this.buttonCount.Text = "Oblicz";
            this.buttonCount.UseVisualStyleBackColor = true;
            this.buttonCount.Click += new System.EventHandler(this.buttonCount_Click);
            // 
            // labelPlus
            // 
            this.labelPlus.AutoSize = true;
            this.labelPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlus.Location = new System.Drawing.Point(556, 108);
            this.labelPlus.Name = "labelPlus";
            this.labelPlus.Size = new System.Drawing.Size(28, 29);
            this.labelPlus.TabIndex = 12;
            this.labelPlus.Text = "+";
            // 
            // labelEqual
            // 
            this.labelEqual.AutoSize = true;
            this.labelEqual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEqual.Location = new System.Drawing.Point(696, 108);
            this.labelEqual.Name = "labelEqual";
            this.labelEqual.Size = new System.Drawing.Size(28, 29);
            this.labelEqual.TabIndex = 13;
            this.labelEqual.Text = "=";
            // 
            // timerCounter
            // 
            this.timerCounter.Interval = 1;
            this.timerCounter.Tick += new System.EventHandler(this.timerCounter_Tick);
            // 
            // textBoxTimer
            // 
            this.textBoxTimer.Location = new System.Drawing.Point(450, 248);
            this.textBoxTimer.Name = "textBoxTimer";
            this.textBoxTimer.Size = new System.Drawing.Size(100, 22);
            this.textBoxTimer.TabIndex = 14;
            // 
            // buttonNewWindowShow
            // 
            this.buttonNewWindowShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonNewWindowShow.Location = new System.Drawing.Point(450, 324);
            this.buttonNewWindowShow.Name = "buttonNewWindowShow";
            this.buttonNewWindowShow.Size = new System.Drawing.Size(146, 64);
            this.buttonNewWindowShow.TabIndex = 15;
            this.buttonNewWindowShow.Text = "Show";
            this.buttonNewWindowShow.UseVisualStyleBackColor = true;
            this.buttonNewWindowShow.Click += new System.EventHandler(this.buttonNewWindowShow_Click);
            // 
            // FormName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 530);
            this.Controls.Add(this.buttonNewWindowShow);
            this.Controls.Add(this.textBoxTimer);
            this.Controls.Add(this.labelEqual);
            this.Controls.Add(this.labelPlus);
            this.Controls.Add(this.buttonCount);
            this.Controls.Add(this.textBoxNumber3);
            this.Controls.Add(this.textBoxNumber2);
            this.Controls.Add(this.textBoxNumber1);
            this.Controls.Add(this.textBoxDigit);
            this.Controls.Add(this.labelDigit);
            this.Controls.Add(this.buttonChange);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Name = "FormName";
            this.Text = "Main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Button buttonChange;
        private System.Windows.Forms.Label labelDigit;
        private System.Windows.Forms.TextBox textBoxDigit;
        private System.Windows.Forms.TextBox textBoxNumber1;
        private System.Windows.Forms.TextBox textBoxNumber2;
        private System.Windows.Forms.TextBox textBoxNumber3;
        private System.Windows.Forms.Button buttonCount;
        private System.Windows.Forms.Label labelPlus;
        private System.Windows.Forms.Label labelEqual;
        private System.Windows.Forms.Timer timerCounter;
        private System.Windows.Forms.TextBox textBoxTimer;
        private System.Windows.Forms.Button buttonNewWindowShow;
    }
}

