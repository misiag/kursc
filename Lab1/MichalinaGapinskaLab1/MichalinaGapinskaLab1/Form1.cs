﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MichalinaGapinskaLab1
{
    public partial class FormName : Form
    {
        /// <summary>
        /// Zmienna przechowująca wartość licznik
        /// </summary>
        int counter = 0;

        FormFunction formFunction;

        public FormName()
        {
            InitializeComponent();
            timerCounter.Start();
        }

        /// <summary>
        /// Funkcja obsługująca przycisk Start
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.AliceBlue;
            buttonStart.BackColor = Color.Bisque;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            buttonStart.BackColor = Color.Red;
            buttonStart.Text = "Nazwa";
        }

       
        /// <summary>
        /// Zmiana tekstu na kolejne wartości (było)
        /// Zmiana tekstu na wpisaną liczbę
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChange_Click(object sender, EventArgs e)
        {
            textBoxResult.Text = "Wartość: " + counter;
            counter = counter + Int32.Parse(textBoxDigit.Text);
        }

        /// <summary>
        /// Dodawanie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCount_Click(object sender, EventArgs e)
        {
            int result = Int32.Parse(textBoxNumber1.Text) + Int32.Parse(textBoxNumber2.Text);
            textBoxNumber3.Text = result.ToString();
        }

        /// <summary>
        /// Obsługa zdarzenia Timera
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerCounter_Tick(object sender, EventArgs e)
        {
            counter++;
            textBoxTimer.Text = counter.ToString();
            if (counter%10 == 0)
            {
                BackColor = Color.Blue;
            }
            else if (counter%10 == 5) {
                BackColor = Color.Red;
            }
        }

        private void buttonNewWindowShow_Click(object sender, EventArgs e)
        {
            formFunction = new FormFunction();
            formFunction.Show();
        }
    }
}
